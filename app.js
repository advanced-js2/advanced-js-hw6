async function fetchIpInfo() {
    try {
        
        const ipifyResponse = await fetch('https://api.ipify.org/?format=json');
        const ipifyData = await ipifyResponse.json();
        const clientIp = ipifyData.ip;
      

        const ipApiResponse = await fetch(`http://ip-api.com/json/${clientIp}`);
       
        const ipApiData = await ipApiResponse.json();
  
        const ipInfoContainer = document.getElementById('ipInfo');
        ipInfoContainer.innerHTML = `
            <p>Континент: ${ipApiData.timezone}</p>
            <p>Країна: ${ipApiData.country}</p>
            <p>Регіон: ${ipApiData.regionName}</p>
            <p>Місто: ${ipApiData.city}</p>
            <p>Район: ${ipApiData.region}</p>
        `;
    } catch (error) {
        console.error('Помилка при отриманні інформації:', error);
    }
}

document.getElementById('findIpButton').addEventListener('click', fetchIpInfo);